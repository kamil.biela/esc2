import express from 'express';
import * as routes from './routes';
import * as bodyParser from 'body-parser';
import * as http from 'http';

export class App {
  private app: express.Express;
  private server: http.Server;

  private configureRoutes(): void {
    if (!this.app) {
      throw new Error('this.app not initialized');
    }

    this.app.get('/GET', routes.mainEndpoint());
  }

  private async initHttp(): Promise<void> {
    if (this.server) {
      throw new Error('Http server already initialized')
    }

    return new Promise(resolve => {
      this.app = express();
      this.app.use(bodyParser.json());

      this.configureRoutes();

      this.server = this.app.listen(8080, resolve);
    });
  }

  async stop(): Promise<void> {
    return new Promise(resolve => {
      if (!this.app) {
        return;
      }
      this.server.once('close', () => {
        resolve();
      });
      this.server.close();
    });
  }

  async start(): Promise<void> {
    return this.initHttp();
  }
}

const app = new App();
app.start().then(() => console.log('Started on port 8080'), console.error);