import * as express from 'express';

export function mainEndpoint(): express.RequestHandler[] {
  const handler: express.RequestHandler = (req, res, next) => {
    (async (): Promise<void> => {
      const value = parseInt(req.query.value, 10);

      if (isNaN(value)) {
        res.send(400);
        return;
      }

      let isP = false;
      let isE = false;

      if (value % 3 === 0) {
        isP = true;
      }

      if (value % 5 === 0) {
        isE = true;
      }

      if (isP === false && isE === false) {
        res.json({data: value});
        return;
      }

      res.json({
        'data': (isP ? 'P' : '') + (isE ? 'E' : '')
      });
    })().catch(next);
  }

  return [handler]
}

export default mainEndpoint;
