import { mainEndpoint } from "../src/routes";

const generateRequestObj = (queryParams: Object): any => {
  return {
    query: {...queryParams},
  };
}

let res: any;
let mockCb: jest.Mock;
let next: jest.Mock;

describe('endpoint.test.ts', function() {
  beforeEach(() => {
    mockCb = jest.fn();
    next = jest.fn();

    res = {
      json: mockCb
    }
  });

  it('divide by 3, result P', async function() {
    const endpoint = mainEndpoint()[0];
    await endpoint(generateRequestObj({value: 3}), res, next);
    expect(mockCb.mock.calls.length).toBe(1);
    expect(mockCb.mock.calls[0][0]).toStrictEqual({data: 'P'});
  });

  // @todo more tests, running out of time
})
